//
//  InterfaceController.swift
//  newInputDemo WatchKit Extension
//
//  Created by jatin verma on 2019-10-16.
//  Copyright © 2019 jatin verma. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    @IBOutlet weak var response: WKInterfaceLabel!
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    @IBAction func buttonClicked() {
    
    let suggestedResponses = ["In class", "Doing Assignment", "At movies", "Sleeping"]
            presentTextInputController(withSuggestions: suggestedResponses, allowedInputMode: .plain) {

        (results) in
                
               
       if (results != nil && results!.count > 0) {
          // 2. write your code to process the person's response
        let userResponse = results?.first as? String
                      self.response.setText(userResponse)

       }
    }

    
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
